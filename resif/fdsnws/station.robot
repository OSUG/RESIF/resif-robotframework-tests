
*** Settings ***
Documentation    Cette suite de tests doit valider le fonctionnement du webservice fdsnws-station

Library        RequestsLibrary
Library        XML
Suite Setup    Create Session    fdsnws-stationxml    http://ws.resif.fr/fdsnws/station/1/
Default Tags   stationxml    functional-test


*** Test Cases ***
User can get metadata for FR in text
    Get ON Session    fdsnws-stationxml    /query  params=net=FR&level=network&format=text

User can get metadata for FR in XML
    ${net_fr_xml}=    Get ON Session    fdsnws-stationxml    /query  params=net=FR&level=network&format=xml
    ${root}=          ParseXML          ${net_fr_xml.content}
    Should Be Equal   ${root.tag}       FDSNStationXML

Get all netwoks objects
    ${nets_xml}=      Get ON Session    fdsnws-stationxml    /query  params=level=network&format=xml
    ${root}=          ParseXML          ${nets_xml.content}
    @{all_networks}=  Get Elements      ${root}    Network[@restrictedStatus="open"]

Post parameters
    [Documentation]    Tester le comportement avec des requêtes POST
    No operation

Sélection selon la localisation géographique
    [Documentation]    Tester la selection avec des paramètres de geolocalisation
    No operation

All networks have an Identifier (DOI)
    [Tags]    optional
    FOR  ${net}  IN  @{all_networks}
        Log     ${net.attrib['code']}
        Element Should Exist    ${net}    Identifier
    END
