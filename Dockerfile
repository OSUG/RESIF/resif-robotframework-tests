FROM python:3.9
COPY Pipfile* /
RUN pip install pipenv
RUN pipenv install
COPY resif /robot/resif
CMD pipenv run robot -n optional -d /pages /robot/resif
