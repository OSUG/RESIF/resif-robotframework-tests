# Tests Resif avec robotframework

## Utilisation

  pipenv install
  pipenv shell
  robot -n optional .
  
## Services à tester

- [] fdsnws/station
- [] fdsnws/dataselect
- [] fdsnws/availability
- [] resifws/statistics
- [] seismology/networks

etc.
